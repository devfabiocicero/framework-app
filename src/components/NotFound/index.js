import React from "react";
import { Result, Button } from "antd";
import { Link } from "react-router-dom";

const NotFound = () => (
  <Result
    status="404"
    title="404"
    subTitle="Desculpe, a página visitada não existe."
    extra={
      <Button type="primary">
        <Link to="/">Voltar para início</Link>
      </Button>
    }
  />
);

export default NotFound;
