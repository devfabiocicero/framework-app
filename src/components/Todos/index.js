import React, { Component } from "react";
import { connect } from "react-redux";

import CardList from "../CardList";
import ErrorMessage from "../ErrorMessage";
import { fetchTodos } from "../../actions";

class Todos extends Component {
  componentDidMount() {
    this.props.fetchTodos();
  }

  render() {
    const { todos, error } = this.props;

    return (
      <div>
        {error && <ErrorMessage errorMsg={error} />}
        <CardList list={todos} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    todos: state.todos.list,
    error: state.todos.error
  };
};

export default connect(mapStateToProps, { fetchTodos })(Todos);
