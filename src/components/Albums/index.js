import React, { Component } from "react";
import { connect } from "react-redux";
import CardList from "../CardList";
import ErrorMessage from "../ErrorMessage";
import { fetchAlbums } from "../../actions";

class Albums extends Component {
  componentDidMount() {
    this.props.fetchAlbums();
  }

  render() {
    const { albums, error } = this.props;

    return (
      <div>
        {error && <ErrorMessage errorMsg={error} />}
        <CardList list={albums} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    albums: state.albums.list,
    error: state.albums.error
  };
};

export default connect(mapStateToProps, { fetchAlbums })(Albums);
