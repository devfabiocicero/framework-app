import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchUser } from "../../actions";
import UserHeader from "./UserHeader";

class User extends Component {
  componentDidMount() {
    this.props.fetchUser(this.props.userId);
  }

  render() {
    const { user } = this.props;

    return <UserHeader user={user} />;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.users.find(user => user.id === ownProps.userId)
  };
};

export default connect(mapStateToProps, { fetchUser })(User);
