import React from "react";
import "./UserHeader.css";

const UserHeader = ({ user }) => (
  <div className="framework-user-header">
    {user && <span>Autor: {user.name}</span>}
  </div>
);

export default UserHeader;
