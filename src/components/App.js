import React from "react";
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";
import { Layout, Menu, Icon } from "antd";
import "./App.css";

import Home from "./Home";
import Todos from "./Todos";
import Posts from "./Posts";
import Albums from "./Albums";
import NotFound from "./NotFound";

const { Content, Footer, Sider } = Layout;

const App = () => (
  <div className="framework-app">
    <BrowserRouter>
      <Layout>
        <Sider
          style={{
            left: 0
          }}
          breakpoint="lg"
          collapsedWidth="0"
        >
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1">
              <Link to="/">
                <Icon type="home" />
                <span className="nav-text">Home</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/todos">
                <Icon type="menu" />
                <span className="nav-text">Todos</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/posts">
                <Icon type="read" />
                <span className="nav-text">Postagens</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link to="/albums">
                <Icon type="picture" />
                <span className="nav-text">Albuns</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Content style={{ margin: "24px 16px 0" }}>
            <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/todos" component={Todos} />
                <Route path="/posts" component={Posts} />
                <Route path="/albums" component={Albums} />
                <Route component={NotFound} />
              </Switch>
            </div>
            <Footer style={{ textAlign: "center" }}>Framework System</Footer>
          </Content>
        </Layout>
      </Layout>
    </BrowserRouter>
  </div>
);

export default App;
