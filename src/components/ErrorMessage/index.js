import React from "react";
import "./ErrorMessage.css";

const ErrorMessage = ({ errorMsg }) => (
  <div>
    <p className="framework-app-errorMsg">{errorMsg}</p>
  </div>
);

export default ErrorMessage;
