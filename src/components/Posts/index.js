import React, { Component } from "react";
import { connect } from "react-redux";

import PostsList from "./PostsList";
import ErrorMessage from "../ErrorMessage";
import { fetchPosts } from "../../actions";

class Posts extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    const { posts, error } = this.props;

    return (
      <div>
        {error && <ErrorMessage errorMsg={error} />}

        <PostsList posts={posts} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts.list,
    error: state.posts.error
  };
};

export default connect(mapStateToProps, { fetchPosts })(Posts);
