import React from "react";
import { Card } from "antd";
import "./PostItem.css";
import User from "../User";

const PostItem = ({ post }) => (
  <Card type="inner" title={post.title} className="framework-app-post-item">
    {post.body}
    <User userId={post.userId} />
  </Card>
);

export default PostItem;
