import React from "react";

import PostItem from "./PostItem";
import { errorNoContent } from "../../helpers/errorMessages";
import ErrorMessage from "../ErrorMessage";

const PostsList = ({ posts }) => {
  const postsRendered = () => {
    return posts.map(post => {
      return <PostItem key={post.id} post={post} />;
    });
  };

  return (
    <div>
      {!!posts.length === false && <ErrorMessage errorMsg={errorNoContent} />}
      {postsRendered()}
    </div>
  );
};

export default PostsList;
