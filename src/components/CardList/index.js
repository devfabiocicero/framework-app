import React from "react";
import { List, Card } from "antd";

import User from "../User";
import { errorNoContent } from "../../helpers/errorMessages";
import ErrorMessage from "../ErrorMessage";

const CardList = ({ list }) => {
  if (!!list.length === false) {
    return <ErrorMessage errorMsg={errorNoContent} />;
  }
  return (
    <List
      grid={{
        gutter: 16,
        xs: 1,
        sm: 2,
        md: 4,
        lg: 4,
        xl: 6,
        xxl: 3
      }}
      dataSource={list}
      renderItem={item => (
        <List.Item>
          <Card title={item.title}>
            <User userId={item.userId} />
          </Card>
        </List.Item>
      )}
    />
  );
};

export default CardList;
