import React from "react";
import "./Home.css";
import FwEntrance from "../../assets/images/framework-entrance.jpg";

const Home = () => (
  <div className="framework-app-home">
    <img src={FwEntrance} alt="Entrada da Framework System" />
  </div>
);

export default Home;
