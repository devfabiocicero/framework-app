import { combineReducers } from "redux";

import postsReducer from "./postsReducer";
import usersReducer from "./usersReducer";
import todosReducer from "./todosReducer";
import albumsReducer from "./albumsReducer";

export default combineReducers({
  posts: postsReducer,
  users: usersReducer,
  todos: todosReducer,
  albums: albumsReducer
});
