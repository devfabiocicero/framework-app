const initialState = {
  list: [],
  error: ""
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_POSTS_SUCCESS":
      return { list: action.payload, error: "" };
    case "FETCH_POSTS_FAILURE":
      return { ...state, error: action.error };
    default:
      return state;
  }
};

export default postsReducer;
