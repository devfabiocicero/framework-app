const initialState = {
  list: [],
  error: ""
};

const todosReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_TODOS_SUCCESS":
      return { list: action.payload, error: "" };
    case "FETCH_TODOS_FAILURE":
      return { ...state, error: action.error };
    default:
      return state;
  }
};

export default todosReducer;
