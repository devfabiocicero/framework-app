const initialState = {
  list: [],
  error: ""
};

const albumsReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_ALBUMS_SUCCESS":
      return { list: action.payload, error: "" };
    case "FETCH_ALBUMS_FAILURE":
      return { ...state, error: action.error };
    default:
      return state;
  }
};

export default albumsReducer;
