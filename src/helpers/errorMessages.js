export const errorApi =
  "Ocorreu um erro com o serviço do JSONPlaceholder, por favor tente novamente mais tarde.";

export const errorNoContent = "Não há dados para serem exibidos.";
