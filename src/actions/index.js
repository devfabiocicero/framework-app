import jsonPlaceholder from "../apis/jsonPlaceholder";
import _ from "lodash";
import { errorApi } from "../helpers/errorMessages";

export const fetchUser = userId => {
  return (dispatch, getState) => {
    _fetchUser(userId, dispatch);
  };
};

// Memoizing the function to not do the request with the same argument
const _fetchUser = _.memoize(async (userId, dispatch) => {
  const response = await jsonPlaceholder.get(`/users/${userId}`);

  dispatch({ type: "FETCH_USER", payload: response.data });
});

export const fetchPosts = () => {
  return async (dispatch, getState) => {
    try {
      const response = await jsonPlaceholder.get("/posts");

      dispatch({ type: "FETCH_POSTS_SUCCESS", payload: response.data });
    } catch (err) {
      dispatch({ type: "FETCH_POSTS_FAILURE", error: errorApi });
    }
  };
};

export const fetchTodos = () => {
  return async (dispatch, getState) => {
    try {
      const response = await jsonPlaceholder.get("/todos");

      dispatch({ type: "FETCH_TODOS_SUCCESS", payload: response.data });
    } catch (err) {
      dispatch({ type: "FETCH_TODOS_FAILURE", error: errorApi });
    }
  };
};

export const fetchAlbums = () => {
  return async (dispatch, getState) => {
    try {
      const response = await jsonPlaceholder.get("/albums");

      dispatch({ type: "FETCH_ALBUMS_SUCCESS", payload: response.data });
    } catch (err) {
      dispatch({ type: "FETCH_ALBUMS_FAILURE", error: errorApi });
    }
  };
};
